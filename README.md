# A green vehicle routing problem with time windows considering the heterogeneous fleet of vehicles: Two meta-heuristic algorithms #

### What is this repository for? ###

This is a repository for the input data of all random instances generated in this paper. The interested readers can use the following data for any future comparison. Moreover, this repository includes the solutions found by the CPLEX solver, the simple Benders Decomposition algorithm, and the Benders Decomposition algorithm with accelerators.

First, we have provided the specification of all 20 instances generated for this problem in the following.

- Instance 1:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%201.txt

- Instance 2:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%202.txt

- Instance 3:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%203.txt

- Instance 4:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%204.txt

- Instance 5:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%205.txt

- Instance 6:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%206.txt

- Instance 7:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%207.txt

- Instance 8:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%208.txt

- Instance 9:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%209.txt

- Instance 10:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2010.txt

- Instance 11:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2011.txt

- Instance 12:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2012.txt

- Instance 13:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2013.txt

- Instance 14:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2014.txt

- Instance 15:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2015.txt

- Instance 16:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2016.txt

- Instance 17:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2017.txt

- Instance 18:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2018.txt

- Instance 19:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2019.txt

- Instance 20:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Spec_Instance%2020.txt


Then, we have provided the solutions found by the CPLEX solver, the simple Benders Decomposition algorithm, and the Benders Decompositon algorithm wiht accelerators.

- CPLEX solver:


- The simple Benders Decomposition algorithm


- The Benders Decomposition algorithm with accelerators:
https://bitbucket.org/Pro_Data/1-t-vrp/downloads/Benders%20Decomposition%20with%20accelerators.rar


